package ee.rluk.logback_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class LogbackTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogbackTestApplication.class, args);
    Logger logger = LoggerFactory.getLogger("ee.rluk.logback_test.LogbackTestApplication");
    logger.info("TEST:INFO:Application started");

    }
}
