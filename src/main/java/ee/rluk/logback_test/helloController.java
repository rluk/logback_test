package ee.rluk.logback_test;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@EnableAutoConfiguration

public class helloController {
    @RequestMapping("/hello")
    @ResponseBody
    public String sayHello() {
        Logger logger = LoggerFactory.getLogger("ee.rluk.logback_test.LogbackTestApplication");
        logger.error("TEST:ERROR:site visited");
        logger.info("TEST:INFO:site visited");
        logger.debug("TEST:DEBUG:site visited");
        return "hello from logback_test feature 1 !!! ";
    }

}
